let canFill = false;
function autoFillWithSingpass() {
  setInterval(() => {
    const iframe = document.querySelector('iframe[title="widget_xcomponent"]');
    const iframeContent = iframe.contentWindow.document;

    var input = iframeContent.querySelector('input[name="driver_full_name_per_nric/fin/passport"]');
    var nativeInputValueSetter = Object.getOwnPropertyDescriptor(window.HTMLInputElement.prototype, "value").set;
    nativeInputValueSetter.call(input, 'John Doe');
    var ev1 = new Event('input', { bubbles: true });
    input.dispatchEvent(ev1);
    // --------------------
    var input = iframeContent.querySelector('input[name="driver_license_number_local_or_foreign_license_number"]');
    var nativeInputValueSetter = Object.getOwnPropertyDescriptor(window.HTMLInputElement.prototype, "value").set;
    nativeInputValueSetter.call(input, '123456');
    var ev2 = new Event('input', { bubbles: true });
    input.dispatchEvent(ev2);
    // --------------------
    var input = iframeContent.querySelector('input[name="international_driving_permit_required_with_foreign_license_number"]');
    var nativeInputValueSetter = Object.getOwnPropertyDescriptor(window.HTMLInputElement.prototype, "value").set;
    nativeInputValueSetter.call(input, 'IDP123');
    var ev3 = new Event('input', { bubbles: true });
    input.dispatchEvent(ev3);
    canFill = true;
  }, 1000)
}
autoFillWithSingpass()
if (canFill) {
  clearInterval();
}

const iframe = document.querySelector('iframe[title="widget_xcomponent"]');
const iframeContent = iframe.contentWindow.document;

var input = iframeContent.querySelector('input[name="driver_full_name_per_nric/fin/passport"]');
var nativeInputValueSetter = Object.getOwnPropertyDescriptor(window.HTMLInputElement.prototype, "value").set;
nativeInputValueSetter.call(input, 'John Doe');
var ev1 = new Event('input', { bubbles: true});
input.dispatchEvent(ev1);
// --------------------
var input = iframeContent.querySelector('input[name="driver_license_number_local_or_foreign_license_number"]');
var nativeInputValueSetter = Object.getOwnPropertyDescriptor(window.HTMLInputElement.prototype, "value").set;
nativeInputValueSetter.call(input, '123456');
var ev2 = new Event('input', { bubbles: true});
input.dispatchEvent(ev2);
// --------------------
var input = iframeContent.querySelector('input[name="international_driving_permit_required_with_foreign_license_number"]');
var nativeInputValueSetter = Object.getOwnPropertyDescriptor(window.HTMLInputElement.prototype, "value").set;
nativeInputValueSetter.call(input, 'IDP123');
var ev3 = new Event('input', { bubbles: true});
input.dispatchEvent(ev3);
// --------------------
var input = iframeContent.querySelector('input[name="driver_country_code"]');
var nativeInputValueSetter = Object.getOwnPropertyDescriptor(window.HTMLInputElement.prototype, "value").set;
nativeInputValueSetter.call(input, 'SG');
var ev4 = new Event('input', { bubbles: true});
input.dispatchEvent(ev4);
// --------------------
var input = iframeContent.querySelector('input[name="driver_contact_number"]');
var nativeInputValueSetter = Object.getOwnPropertyDescriptor(window.HTMLInputElement.prototype, "value").set;
nativeInputValueSetter.call(input, '91234567');
var ev5 = new Event('input', { bubbles: true});
input.dispatchEvent(ev6);
// --------------------
var input = iframeContent.querySelector('input[name="driver_email"]');
var nativeInputValueSetter = Object.getOwnPropertyDescriptor(window.HTMLInputElement.prototype, "value").set;
nativeInputValueSetter.call(input, 'test@gmail.com');
var ev6 = new Event('input', { bubbles: true});
input.dispatchEvent(ev6);
// --------------------
var input = iframeContent.querySelector('input[name="additional_driver_1_name_per_nric/fin/passport"]');
var nativeInputValueSetter = Object.getOwnPropertyDescriptor(window.HTMLInputElement.prototype, "value").set;
nativeInputValueSetter.call(input, 'Jane Doe');
var ev5 = new Event('input', { bubbles: true});
input.dispatchEvent(ev6);
// --------------------
var input = iframeContent.querySelector('input[name="additional_driver_1_driver_license_number_local_or_foreign_license_number"]');
var nativeInputValueSetter = Object.getOwnPropertyDescriptor(window.HTMLInputElement.prototype, "value").set;
nativeInputValueSetter.call(input, '789012');
var ev5 = new Event('input', { bubbles: true});
input.dispatchEvent(ev5);
// --------------------
var input = iframeContent.querySelector('input[name="additional_driver_2_name_per_nric/fin/passport"]');
var nativeInputValueSetter = Object.getOwnPropertyDescriptor(window.HTMLInputElement.prototype, "value").set;
nativeInputValueSetter.call(input, 'Alice Smith');
var ev6 = new Event('input', { bubbles: true});
input.dispatchEvent(ev6);
// --------------------
var input = iframeContent.querySelector('textarea[name="additional_driver_1_address"]');
var nativeInputValueSetter = Object.getOwnPropertyDescriptor(window.HTMLTextAreaElement.prototype, "value").set;
nativeInputValueSetter.call(input, 'Alice Smith');
var ev7 = new Event('textarea', { bubbles: true});
input.dispatchEvent(ev7);


document.querySelector('input[name="additional_driver_2_driver_license_number_local_or_foreign_license_number"]').setAttribute("value",'345678');
document.querySelector('input[name="next_of_kin_name"]').setAttribute("value",'ABC123');
document.querySelector('input[name="next_of_kin_country_code"]').setAttribute("value",'ABC123');
document.querySelector('input[name="next_of_kin_contact_number"]').setAttribute("value",'ABC123');
document.querySelector('input[name="relationship"]').setAttribute("value",'OWNER');
document.querySelector('input[name="additional_driver_1_address"]').setAttribute("value",'OWNER');
document.querySelector('textarea[name="additional_driver_1_address"]').setAttribute("value",'OWNER');
document.querySelector('input[name="additional_driver_1_contact_no."]').setAttribute("value",'OWNER');
document.querySelector('textarea[name="addtional_driver_1_contact_no."]').setAttribute("value",'OWNER');
document.querySelector('input[name="addtional_driver_1_contact_no."]').setAttribute("value",'OWNER');
document.querySelector('textarea[name="additional_driver_2_address"]').setAttribute("value",'OWNER');
document.querySelector('input[name="addtional_driver_2_contact_no."]').setAttribute("value",'OWNER');