const express = require('express');
const path = require('path');
const cors = require('cors');
const bodyParser = require('body-parser');
const app = express();
const port = process.env.PORT || 3001;
const cookieParser = require("cookie-parser");
const router = require('./routes');

// Serve static files from the 'public' directory
app.use(bodyParser.json())
app.use(cors({}));
app.use(express.static(path.join(__dirname, 'public')));
app.use(cookieParser());

app.use(router);

// Start the server
app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
