const express = require('express');
const { validateCallbackFromShopify, checkDriverIsEligible } = require('../utils');
const router = express.Router()

router.get('/app_proxy/singpass-callback', async (req, res, next) => {
  const query = req.query;
  console.log('query----', query);
  const isValid = validateCallbackFromShopify(req.query);
  if(!isValid) {
    res.json({ errrorCode: "INVALID_CALLBACK" })
  }
  const htmlWithoutSingpass = `<!DOCTYPE html><html><head>  <title>Singpass</title></head><body>  <script type="text/javascript">      </script></body></html>`
  if (!req.query.data && !req.query.error) {
    res.setHeader('Content-type', 'text/html')
    res.send(Buffer.from(htmlWithoutSingpass));
    return;
  }
  // Handle error from Singpass
  if (req.query.error) {
    const htmlWithSingpassCallbackError = `<!DOCTYPE html><html><head>  <title>Singpass</title></head><body>  <script type="text/javascript">    function setCookie(name, value, days) {      var expires = "";      if (days) {        var date = new Date();        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));        expires = "; expires=" + date.toUTCString();      }      document.cookie = name + "=" + (value || "") + expires + "; path=/";    }    setCookie('singpass_callback_error_cookie', 'replace_this_cookie', 7);      </script></body></html>`;
    const singpassCallbackErrorData = { ...req.query.error };
    let modifiedContent = htmlWithSingpassCallbackError.replace('replace_this_cookie', JSON.stringify(singpassCallbackErrorData));
    res.setHeader('Content-type', 'text/html')
    res.send(Buffer.from(modifiedContent));
    return;
  }

  // Handle if user is not eligible
  const useValidateErrorCode = checkDriverIsEligible(req.query.data);
  const htmlWithSingpassValidateError = `<!DOCTYPE html><html><head>  <title>Singpass</title></head><body>  <script type="text/javascript">    function setCookie(name, value, days) {      var expires = "";      if (days) {        var date = new Date();        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));        expires = "; expires=" + date.toUTCString();      }      document.cookie = name + "=" + (value || "") + expires + "; path=/";    }    setCookie('singpass_validate_error_cookie', 'replace_this_cookie', 7);      </script></body></html>`;
  if (useValidateErrorCode) {
    const validationErrorResp = { message: "Driver is not eligible", errorCode: "INELIGIBLE_DRIVER" };
    validationErrorResp.errorCode = useValidateErrorCode;
    switch (useValidateErrorCode) {
      case "NOT_ENOUGH_VALIDATE_INFORMATION":
        validationErrorResp.message = "Missing some information to validate";
        break;
      case "NOT_OLD_ENOUGH_TO_DRIVE":
        validationErrorResp.message = "Driver is not old enough to drive";
        break;
      case "NOT_ENOUGH_DRIVING_EXPERIENCE":
        validationErrorResp.message = "Driver does not have enough driving experience";
        break;
      case "INVALID_DRIVING_LICENSE_CLASS":
        validationErrorResp.message = "Driver does not have valid driving license class";
        break;
      default:
        break;
    }
    let modifiedContent = htmlWithSingpassValidateError.replace('replace_this_cookie', JSON.stringify(validationErrorResp));
    res.setHeader('Content-type', 'text/html')
    res.send(Buffer.from(modifiedContent));
    return;
  }

  // Handle verify success
  const htmlWithSingpassSuccess = `<!DOCTYPE html><html><head>  <title>Singpass</title></head><body>  <script type="text/javascript">    function setCookie(name, value, days) {      var expires = "";      if (days) {        var date = new Date();        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));        expires = "; expires=" + date.toUTCString();      }      document.cookie = name + "=" + (value || "") + expires + "; path=/";    }    setCookie('singpass_cookie', 'replace_this_cookie', 7);      </script></body></html>`;
  let modifiedContent = htmlWithSingpassSuccess.replace('replace_this_cookie', JSON.stringify(req.query.data));
  res.setHeader('Content-type', 'text/html')
  res.send(Buffer.from(modifiedContent));
  return;
})


router.get('/app_proxy/singpass-callback-on-reject', async (req, res, next) => {
  const htmlWithoutSingpass = `<!DOCTYPE html><html><head>  <title>Singpass</title></head><body>  <script type="text/javascript">      </script></body></html>`
  res.setHeader('Content-type', 'text/html')
  res.send(Buffer.from(htmlWithoutSingpass));
  return;
})

module.exports = router