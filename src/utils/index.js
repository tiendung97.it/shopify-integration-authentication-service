const crypto = require('crypto');

function checkIsGreaterOrEqualWithYear(dateTime, yearToCompare) {
  // Split the DOB string into year, month, and day components
  const [year, month, day] = dateTime.split('-');
  const birthDate = new Date(parseInt(year), parseInt(month) - 1, parseInt(day)); // Month is 0-indexed
  const currentDate = new Date();
  let years = currentDate.getFullYear() - birthDate.getFullYear();
  // Check if the birth date hasn't occurred yet this year
  if (
    currentDate.getMonth() < birthDate.getMonth() ||
    (currentDate.getMonth() === birthDate.getMonth() && currentDate.getDate() < birthDate.getDate())
  ) {
    years--;
  }

  return years >= yearToCompare;
}

function checkDriverIsEligible(singpassData) {
  const validLicenseClasses = ['3', '3A', '3C', '3CA'];
  const eligibleAge = 23;
  const eligibleDriveExp = 2;
  const { dateOfBirth, licenseClass, licenseIssuedDate } = singpassData;
  if (!dateOfBirth || !licenseClass || !licenseIssuedDate) {
    return "NOT_ENOUGH_VALIDATE_INFORMATION";
  }
  if (!checkIsGreaterOrEqualWithYear(dateOfBirth, eligibleAge)) {
    return "NOT_OLD_ENOUGH_TO_DRIVE";
  }
  if (!checkIsGreaterOrEqualWithYear(licenseIssuedDate, eligibleDriveExp)) {
    return "NOT_ENOUGH_DRIVING_EXPERIENCE";
  }
  if (!validLicenseClasses.includes(licenseClass)) {
    return "INVALID_DRIVING_LICENSE_CLASS"
  }
  return null;
}

function objectToSortedStr(obj, prefix) {
  return Object.keys(obj).sort().reduce((total, key) => {
    let nestedPrefix = prefix + `[${key}]`;
    if (typeof obj[key] === 'object') {
      total += objectToSortedStr(obj[key], nestedPrefix);
      return total;
    }
    total += `${prefix}[${key}]=${obj[key]}`;
    return total;
  }, "");
}

function validateCallbackFromShopify(query) {
  const { signature } = query;
  delete query.signature;
  const orderedObj = Object.keys(query).sort().reduce((total, key) => {
    if (typeof query[key] === 'object') {
      total += objectToSortedStr(query[key], key);
      return total;
    }
    total += `${key}=${query[key]}`;
    return total;
  }, "");
  console.log('orderedObj-----', orderedObj);
  const generatedHash = crypto
    .createHmac('sha256', "bd26e678a52883eb78a2d2e2a7f407f5")
    .update(orderedObj, 'utf-8')
    .digest('hex');
  return signature === generatedHash;
}

module.exports = {
  checkDriverIsEligible,
  checkIsGreaterOrEqualWithYear,
  validateCallbackFromShopify
}